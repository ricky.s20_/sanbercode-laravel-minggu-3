<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Tugas Kemaren dan Coba materi
// Route::get('/', 'HomeController@home');

// Route::get('/regis', 'AuthController@registration');

// Route::post('/submit', 'AuthController@form');

// Route::get('/master', function(){
//     return view('adminlte.master');
// });

// Route::get('/items', function(){
//     return view('adminlte.items.index');
// });

Route::get('/', function () {
    return view('master');
});

Route::get('/table', function(){
    return view('table');
});

Route::get('data-table', function(){
    return view('datatable');
});


//Tugas Minggu 3 Hari 5
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');
