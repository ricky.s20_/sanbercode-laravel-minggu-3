@extends('master')
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Nama Cast</h3>
      </div>
    <div class="card-body">
        <a href="/cast/create" class="btn btn-primary">Tambah</a>
        <table table id="example1" class="table table-bordered table-striped">
            <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
                @forelse ($cast as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>{{$value->umur}}</td>
                        <td>{{$value->bio}}</td>
                        <td>
                            <a href="/cast/{{$value->id}}" class="btn btn-info"><i class="fa fa-eye"></i></a>
                            <a href="/cast/{{$value->id}}/edit" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection
